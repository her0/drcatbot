#!/usr/bin/env python3

import re
import requests
import time
import unicodedata
import urllib.parse

account = ''
api_key = ''
room = ''
roompw = ''

banned_words = set()
base_url = 'http://www.icanhazchat.com/api.ashx?v=1&'
polling_rate = 2
room_key = ''

def scompare_prep(string):
	return unicodedata.normalize("NFKD", string.casefold())

def add_banned_words(filename):
	with open(filename) as f:
		global banned_words
		for line in f:
			line = line.strip()
			line = scompare_prep(line)
			banned_words.add(line)

def user_from_line(line):
	try:
		p = re.compile(r'(?:\w{6}\|)(\w+)(?::)')
		m = p.match(line)
		return m.group(1)
	except (Exception):
		pass

def filter(func):
	def word_filter(*args, **kargs):
		text = func(*args, **kargs)
		lines = text.splitlines()
		for l in lines:
			# TODO only check public chat?
			l = scompare_prep(l)
			for w in banned_words:
				if w in l:
					silence(user_from_line(l))
					# TODO log actions
	return word_filter

def join_room():
	global room_key
	if roompw:
		r = requests.get(f'{base_url}u={account}&p={api_key}&a=join&w={room}&password={roompw}')
	else:
		r = requests.get(f'{base_url}u={account}&p={api_key}&a=join&w={room}')
	r_lines = r.text.splitlines()
	if r_lines[0] == 'OK':
		room_key = r_lines[1]
	else:
		raise Exception
		# TODO more robust error handling throughout

@filter
def send(text):
	text = urllib.parse.quote_plus(text)
	r = requests.get(f'{base_url}k={room_key}&a=send&w={text}')
	return r.text

@filter
def recv():
	r = requests.get(f'{base_url}k={room_key}&a=recv')
	return r.text

def selfmod():
	return send('/modme')

def silence(username):
	return send(f'/silence! {username}')

if __name__ == '__main__':
	# TODO add arguments

	add_banned_words('banned_words.txt')

	join_room()
	selfmod()

	# Loop
	while True:
		time.sleep(polling_rate)
		recv()
